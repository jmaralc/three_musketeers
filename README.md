# About me...
Javier Martinez Alcantara, Javi.

[LinkedIn](https://www.linkedin.com/in/javier-martinez-alcantara/)

[Some info](https://jmaralc.github.io/)

# Three_musketeers

The idea of this project is to present three different techniques/tecnologies in the python environment to address one problem, deal with many and costly operations in an API.
Here the 
[SLIDES](https://docs.google.com/presentation/d/1WYbNyjdp4dV2n8F33n-nfJu7mvwlOJoFcgsxhNPciRU/edit?usp=sharing)

# Motivation

Nowadays is not rare to have to develop a REST API to provide service to other services and it is not rare the need to consume from other thirdparty APIs. Contrary to the operations in memory, the oparations or requests in which we have to access network interfaces (as well as with the disk access) have a significant delay.

# Setup

For this talk you will need python 3.8 and pip installed in your computer. We will use pipenv for installing the packages and keeping the virtual environment. So I recomend you to install it with pip:

```shell
pip install --user --upgrade pipenv
```
Further information about installation [here](https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv)

# Global Goal

Increase your knowledge on developing web services based on python with concurrence. 

# Structure of the repository/project

Each one of the branches contains the data we will go through. So we here is a brief description of the branches as we progress with the talk:

* **init** The branch that contains the initial implementation of our API. It is implemented with Flask for the web application and using Gunicorn as web server.

* **async** Branch were we will stop briefly to explain some concepts related to concurrence and why is recommended in some cases. 
* **fastapi** Branch that covers the initial example but now migrated to fastAPI as application server and Uvicorn as web server.
* **celery** Branch that implement the case in which a distributed task could be needed.

# *init* branch
To access this branch just:
```bash
git checkout init
```
Once in this branch we will explain the situation and the details of the implementation.

The most basic command for the execution of the web service is showed below:

```bash
pipenv run gunicorn -w 1 --timeout 300 --bind 0.0.0.0:5000 main:app
```
Perhaps the most important parameters are:
-  ***w*** which in this case is 1. Indicates the number of workers. Consider a worker as a thread in your OS.
- ***timeout*** indicates the time after a request arrival that the server will cancel that request.
- ***bind*** specifies the IP address and the port.

For testing our server we have a client-simulator. To use it, type the commands below in a different console:
```bash
pipenv run python client.py <the number of concurrent users>
pipenv run python client.py 10
```

**MINIGOAL** One realizes that python widespread approaches lack of some features when facing real situations. 

# *async* branch
To access this branch just:
```bash
git checkout async
```
This branch is a break in the presentation unrelated to the original example and just presenting a really simple example to show the mechanics of asyncio.

The main file implement a counter. This counter could be an synchronous one or an asynchronous one. The way to call it:
```bash
pipenv run python main.py
```
This will launch two counters (counting up to 10); first one synchronous and later on an asynchronous. 

During the session we will play a bit with the code to shows ways to improve the execution.

If one wants to run just one version counting up to a different number one has to use:
```bash
pipenv run python main.py <counting up to> <mode s/a>
pipenv run python main.py 25 a
```
Where the first parameters is the digit we want to count to, and the second parameter is the kind of counter (sync/async are the valid values).

**MINIGOAL** One gets the idea that asyncio framework promotes a different but explicit approach to arrange the code, avoiding monkey patching as other solutions like greenlets or eventlets do.

# *fastapi* branch
To access this branch just:
```bash
git checkout fastapi
```
This branch provides you the first steps into the ASGI and fastAPI framework to develop web services. It implements the same endopoints presented in the *init* branch but now in an asynchronous mode. Some other benefits of fastAPI will be also reviewed.

To run the application:
```bash
pipenv run uvicorn main:app --reload
```
As before we have our client-simulator, that in this case accept one extra parameter to measure whether we want to access synchronous or asynchronous endpoints:
```bash
pipenv run python client.py <the number of concurrent users> <mode>
pipenv run python client.py 10 a
```

For benchmarking with Locust, use the next command in a different console:
```bash
pipenv run locust --host=http://localhost:8000
```

**MINIGOAL** One captures the benefits of fastAPI when dealing with implementing concurrence in a web service. 

# *celery* branch
To access this branch just:
```bash
git checkout celery
```
This branch introduces the idea of a asynchronous task queue/manager when we have to deal with long/costly processes that could harm the user experience. When we face operations that take longer than a certain threshold (let say 30 seconds), it is worthy to think about a queue system to deal with them. Celery provides the mechanism to manage this situations, put the tasks in a queue and presist the results so when the user ask for their result our application can say whether the data is ready (and deliver it) or not yet.

This branch requires you have configured redis and set it up in the config.py, so I will show the execution and during the presentation and later on you can set your own redis and home and play with it.

I recommend three consoles open 

In one of the consoles we can start Celery with the command:
```bash
pipenv run celery worker --app celery_app.my_celery --loglevel=info
```
In a different one start our fastAPI application as before:
```bash
pipenv run uvicorn main:app --reload
```

In the third console, run the client. In this case the client is a bit different than before. Instead of waiting for the response. It will get the id of the task assigned to its requests and will start pulling the state of its requests from our fastAPI server until it get SUCCESS state. To run it:

```bash
pipenv run python client.py <the number of concurrent users>
pipenv run python client.py 10 a
```

**MINIGOAL** Integrate costly opearations in an asynchronous manner within our application.

# What from here...

From here you can start different experiments:
- Review the code yourself and make tests based on this repo
- Ask me any question of the code here in gitlab or by email (jmarlac@gmail.com)
- Bench mark your own app with locust and look for places where you could improve with asyncio
- Get familiar with the asyncio Event loop and corutines, tasks and future objects
- Move your flask app to fastAPI
- Deploy Celery together with your app
- Try to use Celery with asyncio
- Try alternatives like Hypercorn with Quart

# Relevant links

[Gunicorn](https://gunicorn.org/)

[Flask](https://flask.palletsprojects.com/en/1.1.x/)

[asyncio](https://docs.python.org/3/library/asyncio.html)

[fastAPI](https://fastapi.tiangolo.com/features/)

[uvicorn](https://www.uvicorn.org/)

[Starlette](https://www.starlette.io/)

[Pydantic](https://bahiz.zeelo.co/)

[Locust](https://locust.io/)

[Celery](https://docs.celeryproject.org/en/stable/)

[Redis](https://redis.io/)
